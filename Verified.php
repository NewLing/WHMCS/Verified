<?php

if (!defined("WHMCS")) {
    die("This file cannot be accessed directly");
}

use WHMCS\Database\Capsule;

require_once __DIR__ . '/api.php';

function verified_config() {
    return [
        'name' => '实名认证',
        'description' => '基于视频活体检测的实名认证。',
        'author' => 'NewLing',
        'version' => '1.0',
        'fields' => [
            'appcode' => [
                'FriendlyName' => 'AppCode',
                'Type' => 'text',
                'Size' => '32',
                'Default' => '',
                'Description' => '请输入您的AppCode。',
            ],
        ]
    ];
}

function verified_activate() {
    try {
        Capsule::schema()->create('mod_verified', function ($table) {
            $table->increments('id');
            $table->integer('userid')->unique();
            $table->string('name');
            $table->string('idno');
            $table->string('video_url');
            $table->string('motion');
            $table->boolean('verified')->default(0);
            $table->text('result');
            $table->timestamps();
        });
    } catch (Exception $e) {
        return [
            'status' => 'error',
            'description' => '无法创建数据库表: ' . $e->getMessage(),
        ];
    }

    return [
        'status' => 'success',
        'description' => '实名认证插件激活成功。',
    ];
}

function verified_deactivate() {
    Capsule::schema()->dropIfExists('mod_verified');

    return [
        'status' => 'success',
        'description' => '实名认证插件已成功卸载。',
    ];
}

function verified_output($vars) {
    $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
    switch ($action) {
        case 'view':
            $userId = $_REQUEST['id'];
            $verificationData = Capsule::table('mod_verified')->where('userid', $userId)->first();
            if ($verificationData) {
                echo '<p><strong>姓名:</strong> ' . $verificationData->name . '</p>';
                echo '<p><strong>身份证号:</strong> ' . $verificationData->idno . '</p>';
                echo '<p><strong>视频URL:</strong> ' . $verificationData->video_url . '</p>';
                echo '<p><strong>动作:</strong> ' . $verificationData->motion . '</p>';
                echo '<p><strong>验证结果:</strong> ' . $verificationData->result . '</p>';
            } else {
                echo "<p>未找到验证信息。</p>";
            }
            break;
        default:
            echo "管理员后台界面还未实现。";
            break;
    }
}

function verified_clientarea($vars) {
    $appCode = $vars['appcode'];
    $uploadPath = __DIR__ . '/tmp/'; // 确保这个目录存在且可写
    if (!is_dir($uploadPath)) {
        mkdir($uploadPath, 0755, true);
    }
    $baseUrl = 'https://wow.lingyunt.top/modules/addons/verified/tmp/';

    $response = [];
    $userId = $_SESSION['uid'];

    // 检查数据库中是否已存在该用户的验证信息
    $existingVerification = Capsule::table('mod_verified')->where('userid', $userId)->first();

    if ($existingVerification && $existingVerification->verified) {
        // 用户已完成实名认证
        $response = [
            'verified' => true,
            'name' => $existingVerification->name,
            'idno' => $existingVerification->idno,
            'successMessage' => "您已完成实名认证。",
        ];
    } else {
        $motions = ['BLINK' => '眨眼', 'MOUTH' => '张嘴', 'NOD' => '点头', 'YAW' => '摇头'];
        $randomKey = array_rand($motions);
        $response['motionDescription'] = $motions[$randomKey];

        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_FILES['video'])) {
            $name = $_POST['name'];
            $idno = $_POST['idno'];
            $video = $_FILES['video'];
            $fileExtension = strtolower(pathinfo($video['name'], PATHINFO_EXTENSION));

            if ($fileExtension !== 'mp4') {
                $response['errorMessage'] = '仅支持MP4格式的视频文件。';
            } else {
                $newFileName = uniqid("video_", true) . '.' . $fileExtension;
                $destination = $uploadPath . $newFileName;

                if (move_uploaded_file($video['tmp_name'], $destination)) {
                    $videoUrl = $baseUrl . $newFileName;

                    // 假设 callLifeFaceCheckAPI 函数已实现
                    $apiResponse = callLifeFaceCheckAPI($videoUrl, $randomKey, $name, $idno, $appCode);

                    if ($apiResponse['success']) {
                        Capsule::table('mod_verified')->updateOrInsert(
                            ['userid' => $userId],
                            [
                                'name' => $name,
                                'idno' => $idno,
                                'video_url' => $videoUrl,
                                'motion' => $randomKey,
                                'verified' => 1,
                                'result' => json_encode($apiResponse['data']),
                            ]
                        );

                        $response = [
                            'verified' => true,
                            'name' => $name,
                            'idno' => $idno,
                            'successMessage' => "实名认证成功。",
                        ];
                    } else {
                        $response['errorMessage'] = "实名认证失败：" . $apiResponse['message'];
                    }
                } else {
                    $response['errorMessage'] = "视频上传失败，请重试。";
                }
            }
        }
    }

    return [
        'pagetitle' => '实名认证',
        'breadcrumb' => ['index.php?m=verified' => '实名认证'],
        'templatefile' => 'verified',
        'requirelogin' => true,
        'vars' => $response,
    ];
}

?>
