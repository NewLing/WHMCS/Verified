{if $verified}
    <p>{$successMessage}</p>
    <p><strong>姓名:</strong> {$name}</p>
    <p><strong>身份证号:</strong> {$idno}</p>
{else}
    {if $errorMessage}
    <div class="alert alert-danger" role="alert">
        {$errorMessage}
    </div>
    {/if}

    <p>请上传完成以下动作的视频：{$motionDescription}</p>

    <form action="{$systemurl}index.php?m=verified" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="name">姓名:</label>
            <input type="text" name="name" id="name" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="idno">身份证号:</label>
            <input type="text" name="idno" id="idno" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="video">视频上传:</label>
            <input type="file" name="video" id="video" class="form-control-file" accept="video/mp4" required>
        </div>
        <button type="submit" class="btn btn-primary">提交认证</button>
    </form>
{/if}
