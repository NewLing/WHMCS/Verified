{foreach from=$verificationData item=data}
    <div>
        <p><strong>姓名:</strong> {$data.name}</p>
        <p><strong>身份证号:</strong> {$data.idno}</p>
        <p><strong>视频URL:</strong> <a href="{$data.video_url}" target="_blank">查看视频</a></p>
        <p><strong>动作:</strong> {$data.motion}</p>
        <p><strong>验证结果:</strong> {$data.result}</p>
    </div>
{/foreach}
