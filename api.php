<?php

function callLifeFaceCheckAPI($url, $motions, $name, $idcard, $appcode) {
    $host = "https://slyhtsrrz.market.alicloudapi.com";
    $path = "/lifeface/check";
    $method = "POST";
    $headers = [
        "Authorization:APPCODE " . $appcode,
        "Content-Type:application/x-www-form-urlencoded; charset=UTF-8"
    ];
    $query = "url=" . urlencode($url) . "&motions=" . $motions . "&name=" . urlencode($name) . "&idcard=" . $idcard;

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_URL, $host . $path . "?" . $query);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    if (1 == strpos("$" . $host, "https://")) {
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    }

    $response = curl_exec($curl);
    if (curl_errno($curl)) {
        $error_msg = curl_error($curl);
        logModuleCall(
            'verified',
            'api_request',
            $curl,
            $response,
            $error_msg
        );
        curl_close($curl);
        return ['success' => false, 'message' => "Curl error: " . $error_msg];
    }

    curl_close($curl);
    logModuleCall(
        'verified',
        'api_request',
        $query,
        $response,
        null
    );

    $result = json_decode($response, true);
    if ($result && $result['success']) {
        return ['success' => true, 'data' => $result];
    } else {
        return ['success' => false, 'message' => $result['msg'] ?? "API调用失败"];
    }
}
